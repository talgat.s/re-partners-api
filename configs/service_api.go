package configs

import (
	"context"
	"flag"
	
	"github.com/sethvargo/go-envconfig"

	"gitlab.com/talgat.s/re-partners-api/internal/constant"
)

type ApiConfig struct {
	*SharedConfig
	Env constant.Environment
}

func newApiConfig(ctx context.Context, env constant.Environment) (*ApiConfig, error) {
	c := &ApiConfig{
		Env: env,
	}

	if err := envconfig.Process(ctx, c); err != nil {
		return nil, err
	}

	flag.IntVar(&c.Port, "port", c.Port, "server port [PORT]")

	return c, nil
}
