package main

import (
	"context"

	"gitlab.com/talgat.s/re-partners-api/cmd/api/server"
	"gitlab.com/talgat.s/re-partners-api/cmd/database/model"
	"gitlab.com/talgat.s/re-partners-api/configs"
	"gitlab.com/talgat.s/re-partners-api/internal/constant"
	"gitlab.com/talgat.s/re-partners-api/pkg/logger"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	// conf
	conf, err := configs.NewConfig(ctx)
	if err != nil {
		panic(err)
	}

	// setup logger
	log := logger.New(conf)

	// configure database service
	mdl, err := model.New(ctx, log.With("service", constant.Database), conf.Database)

	if err != nil {
		log.ErrorContext(ctx, "model initialization error", "error", err)
		panic(err)
	}

	// configure api service
	srv := server.New(ctx, log.With("service", constant.Api), conf.Api)
	// start api service
	srv.Start(ctx, cancel, mdl)

	<-ctx.Done()
	// Your cleanup tasks go here
	log.InfoContext(ctx, "cleaning up ...")

	_ = mdl.Close()
	log.InfoContext(ctx, "successfully closed db")

	log.InfoContext(ctx, "server was successfully shutdown.")
}
