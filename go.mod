module gitlab.com/talgat.s/re-partners-api

go 1.21

require (
	github.com/joho/godotenv v1.5.1
	github.com/sethvargo/go-envconfig v0.9.0
)

require github.com/lib/pq v1.10.9 // indirect
