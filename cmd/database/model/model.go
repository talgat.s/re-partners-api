package model

import (
	"context"
	"database/sql"
	"log/slog"

	"gitlab.com/talgat.s/re-partners-api/cmd/database"
	"gitlab.com/talgat.s/re-partners-api/configs"
)

type Model interface {
	Close() error
	SelectAllPacksSizes(ctx context.Context) ([]int, error)
	ReplaceAllPacksSizes(ctx context.Context, sizes []int) error
}

type modelService struct {
	log  *slog.Logger
	conf *configs.DatabaseConfig
	db   *sql.DB
}

func New(ctx context.Context, log *slog.Logger, conf *configs.DatabaseConfig) (Model, error) {
	db, err := database.NewDB(
		conf.Host,
		conf.Port,
		conf.User,
		conf.Password,
		conf.DBName,
	)

	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		return nil, err
	}

	log.InfoContext(ctx, "initialize database service")

	m := &modelService{
		log:  log,
		conf: conf,
		db:   db,
	}

	return m, nil
}

func (mdl *modelService) Close() error {
	return mdl.db.Close()
}
