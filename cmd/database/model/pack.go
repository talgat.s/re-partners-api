package model

import (
	"context"
)

func (mdl *modelService) SelectAllPacksSizes(ctx context.Context) ([]int, error) {
	mdl.log.InfoContext(ctx, "start SelectAllPacksSizes")

	sizes := make([]int, 0)

	rows, err := mdl.db.Query("SELECT size FROM pack ORDER BY size")
	if err != nil {
		mdl.log.ErrorContext(ctx, "error SelectAllPacksSizes", "error", err)
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var size int

		if err = rows.Scan(&size); err != nil {
			mdl.log.ErrorContext(ctx, "error SelectAllPacksSizes", "error", err)
			return nil, err
		} else {
			sizes = append(sizes, size)
		}
	}

	if err = rows.Err(); err != nil {
		mdl.log.ErrorContext(ctx, "error SelectAllPacksSizes", "error", err)
		return nil, err
	}

	mdl.log.InfoContext(ctx, "success SelectAllPacksSizes", "data", sizes)
	return sizes, nil
}

func (mdl *modelService) ReplaceAllPacksSizes(ctx context.Context, sizes []int) error {
	mdl.log.InfoContext(ctx, "start ReplaceAllPacksSizes")

	txn, err := mdl.db.Begin()
	if err != nil {
		mdl.log.ErrorContext(ctx, "error ReplaceAllPacksSizes", "error", err)
		return err
	}

	_, err = txn.Exec("TRUNCATE pack CASCADE")
	if err != nil {
		_ = txn.Rollback()
		mdl.log.ErrorContext(ctx, "error ReplaceAllPacksSizes", "error", err)
		return err
	}

	stmt, err := txn.Prepare("INSERT INTO pack (size) VALUES ($1)")
	if err != nil {
		_ = txn.Rollback()
		mdl.log.ErrorContext(ctx, "error ReplaceAllPacksSizes", "error", err)
		return err
	}

	for _, size := range sizes {
		if _, err := stmt.Exec(size); err != nil {
			_ = txn.Rollback()
			mdl.log.ErrorContext(ctx, "error ReplaceAllPacksSizes", "error", err)
			return err
		}
	}

	if err := txn.Commit(); err != nil {
		_ = txn.Rollback()
		mdl.log.ErrorContext(ctx, "error ReplaceAllPacksSizes", "error", err)
		return err
	}

	mdl.log.InfoContext(ctx, "success ReplaceAllPacksSizes")
	return nil
}
