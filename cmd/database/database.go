package database

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

func NewDB(host string, port int, user string, password string, dbname string) (*sql.DB, error) {
	psqlInfo := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname,
	)
	return sql.Open("postgres", psqlInfo)
}
