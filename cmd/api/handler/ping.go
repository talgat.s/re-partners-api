package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// Ping handler for all methods
func (h *handlerObject) Ping(w http.ResponseWriter, r *http.Request) {
	h.api.GetLog().InfoContext(r.Context(), "start Ping")

	switch r.Method {
	case http.MethodGet:
		h.getPing(w, r)
	case http.MethodOptions:
		w.Header().Set("Allow", "GET, OPTIONS")
		w.WriteHeader(http.StatusNoContent)
	default:
		mes := fmt.Sprintf("method: %s not allowed", r.Method)
		h.api.GetLog().InfoContext(r.Context(), "error Ping", "error", mes)
		w.Header().Set("Allow", "GET, OPTIONS")
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)
	}
}

// getPing for GET method
func (h *handlerObject) getPing(w http.ResponseWriter, r *http.Request) {
	h.api.GetLog().InfoContext(r.Context(), "start getPing")

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	resp := map[string]interface{}{
		"status":  "success",
		"message": "pong",
	}
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		mes := fmt.Sprintf("error building the response, %v", err)
		h.api.GetLog().InfoContext(r.Context(), "error getPing", "error", mes)
		http.Error(w, mes, http.StatusInternalServerError)
		return
	}
}
