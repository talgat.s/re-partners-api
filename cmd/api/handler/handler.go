package handler

import (
	"net/http"

	"gitlab.com/talgat.s/re-partners-api/cmd/api"
	"gitlab.com/talgat.s/re-partners-api/cmd/database/model"
)

type Handler interface {
	Ping(w http.ResponseWriter, r *http.Request)
	Pack(w http.ResponseWriter, r *http.Request)
	OrderItem(w http.ResponseWriter, r *http.Request)
}

type handlerObject struct {
	api api.Api
	mdl model.Model
}

func New(api api.Api, mdl model.Model) Handler {
	return &handlerObject{
		api: api,
		mdl: mdl,
	}
}
