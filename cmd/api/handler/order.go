package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// OrderItem handler for all methods
func (h *handlerObject) OrderItem(w http.ResponseWriter, r *http.Request) {
	h.api.GetLog().InfoContext(r.Context(), "start Order")

	switch r.Method {
	case http.MethodGet:
		h.getOrder(w, r)
	case http.MethodOptions:
		w.Header().Set("Allow", "GET, OPTIONS")
		w.WriteHeader(http.StatusNoContent)
	default:
		mes := fmt.Sprintf("method: %s not allowed", r.Method)
		h.api.GetLog().InfoContext(r.Context(), "error Order", "error", mes)
		w.Header().Set("Allow", "GET, OPTIONS")
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)
	}
}

// getOrder find orders packs relative to its size
func (h *handlerObject) getOrder(w http.ResponseWriter, r *http.Request) {
	// id := c.Params("id")
	// db := database.DB
	// var user model.User
	// db.Find(&user, id)
	// if user.Username == "" {
	// 	return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No user found with ID", "data": nil})
	// }
	// return c.JSON(fiber.Map{"status": "success", "message": "User found", "data": user})

	h.api.GetLog().InfoContext(r.Context(), "start getOrder")

	sizes := []int{250, 500, 1000, 2000, 5000}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	resp := map[string]interface{}{
		"status":  "success",
		"message": "pong",
		"data": map[string]interface{}{
			"sizes": sizes,
		},
	}
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		mes := fmt.Sprintf("error building the response, %v", err)
		h.api.GetLog().InfoContext(r.Context(), "error getOrder", "error", mes)
		http.Error(w, mes, http.StatusInternalServerError)
		return
	}
}
