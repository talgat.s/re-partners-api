package handler

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/talgat.s/re-partners-api/cmd/api/helpers"
)

// Pack handler for all methods
func (h *handlerObject) Pack(w http.ResponseWriter, r *http.Request) {
	h.api.GetLog().InfoContext(r.Context(), "start Pack")

	switch r.Method {
	case http.MethodGet:
		h.getAllPacks(w, r)
	case http.MethodPut:
		h.replaceAllPacks(w, r)
	case http.MethodOptions:
		w.Header().Set("Allow", "GET, PUT, OPTIONS")
		w.WriteHeader(http.StatusNoContent)
	default:
		mes := fmt.Sprintf("method: %s not allowed", r.Method)
		h.api.GetLog().InfoContext(r.Context(), "error Pack", "error", mes)
		w.Header().Set("Allow", "GET, PUT, OPTIONS")
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)
	}
}

// getAllPacks get all packs sizes
func (h *handlerObject) getAllPacks(w http.ResponseWriter, r *http.Request) {
	h.api.GetLog().InfoContext(r.Context(), "start getAllPacks")

	sizes, err := h.mdl.SelectAllPacksSizes(r.Context())
	if err != nil {
		mes := fmt.Sprintf("error db, %v", err)
		h.api.GetLog().InfoContext(r.Context(), "error getAllPacks", "error", mes)
		http.Error(w, mes, http.StatusInternalServerError)
		return
	}

	resp := map[string]interface{}{
		"status":  "success",
		"message": "fetched all packs",
		"data": map[string]interface{}{
			"sizes": sizes,
		},
	}
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		mes := fmt.Sprintf("error building the response, %v", err)
		h.api.GetLog().InfoContext(r.Context(), "error getAllPacks", "error", mes)
		http.Error(w, mes, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
}

// replaceAllPacks get all packs sizes
func (h *handlerObject) replaceAllPacks(w http.ResponseWriter, r *http.Request) {
	h.api.GetLog().InfoContext(r.Context(), "start replaceAllPacks")

	var body struct {
		Data []int `json:"data"`
	}

	err := helpers.DecodeJSONBody(w, r, &body)
	if err != nil {
		var mr *helpers.MalformedRequest
		if errors.As(err, &mr) {
			http.Error(w, mr.Message, mr.Status)
		} else {
			h.api.GetLog().InfoContext(r.Context(), "error replaceAllPacks", "error", err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
		return
	}

	if err := h.mdl.ReplaceAllPacksSizes(r.Context(), body.Data); err != nil {
		mes := fmt.Sprintf("error db, %v", err)
		h.api.GetLog().InfoContext(r.Context(), "error replaceAllPacks", "error", mes)
		http.Error(w, mes, http.StatusInternalServerError)
		return
	}

	resp := map[string]interface{}{
		"status":  "success",
		"message": "replaced all packs",
	}
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		mes := fmt.Sprintf("error building the response, %v", err)
		h.api.GetLog().InfoContext(r.Context(), "error replaceAllPacks", "error", mes)
		http.Error(w, mes, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
}
