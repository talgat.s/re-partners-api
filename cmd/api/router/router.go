package router

import (
	"net/http"

	"gitlab.com/talgat.s/re-partners-api/cmd/api"
	"gitlab.com/talgat.s/re-partners-api/cmd/api/handler"
	"gitlab.com/talgat.s/re-partners-api/cmd/database/model"
)

// SetupRoutes setup router api
func SetupRoutes(mux *http.ServeMux, api api.Api, mdl model.Model) {
	h := handler.New(api, mdl)

	mux.HandleFunc("/api/ping", h.Ping)
	mux.HandleFunc("/api/pack", h.Pack)
	mux.HandleFunc("/api/order", h.OrderItem)
}
