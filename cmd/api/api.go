package api

import (
	"context"
	"log/slog"

	"gitlab.com/talgat.s/re-partners-api/cmd/database/model"
)

type Api interface {
	Start(ctx context.Context, cancel context.CancelFunc, mdl model.Model)
	GetLog() *slog.Logger
}
