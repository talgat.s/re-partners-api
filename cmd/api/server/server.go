package server

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net"
	"net/http"
	"os"
	"os/signal"

	"gitlab.com/talgat.s/re-partners-api/cmd/api"
	"gitlab.com/talgat.s/re-partners-api/cmd/api/router"
	"gitlab.com/talgat.s/re-partners-api/cmd/database/model"
	"gitlab.com/talgat.s/re-partners-api/configs"
)

type apiService struct {
	log  *slog.Logger
	conf *configs.ApiConfig
}

func New(ctx context.Context, log *slog.Logger, conf *configs.ApiConfig) api.Api {
	a := &apiService{
		log:  log,
		conf: conf,
	}

	log.InfoContext(ctx, "initialize api service")

	return a
}

func (a *apiService) GetLog() *slog.Logger {
	return a.log
}

func (a *apiService) Start(ctx context.Context, cancel context.CancelFunc, mdl model.Model) {
	mux := http.NewServeMux()
	router.SetupRoutes(mux, a, mdl)

	server := &http.Server{
		Addr:    fmt.Sprintf(":%d", a.conf.Port),
		Handler: mux,
		BaseContext: func(_ net.Listener) context.Context {
			return ctx
		},
	}

	// Listen from a different goroutine
	go func() {
		if err := server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			a.log.ErrorContext(ctx, "server error", "error", err)
		}

		cancel()
	}()

	a.log.InfoContext(ctx, "starting server", "PORT", a.conf.Port)

	shutdown := make(chan os.Signal, 1)   // Create channel to signify a signal being sent
	signal.Notify(shutdown, os.Interrupt) // When an interrupt is sent, notify the channel

	go func() {
		<-shutdown

		a.log.WarnContext(ctx, "gracefully shutting down...")
		if err := server.Shutdown(ctx); err != nil {
			a.log.ErrorContext(ctx, "server shutdown error", "error", err)
		}
	}()
}
